/**
 * Copyright (C) 2011 Whisper Systems
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package free.text.sms.sms;

import android.content.Context;
import android.util.Log;

import org.whispersystems.jobqueue.JobManager;

import free.text.sms.ApplicationContext;
import free.text.sms.crypto.MasterSecret;
import free.text.sms.database.DatabaseFactory;
import free.text.sms.database.EncryptingSmsDatabase;
import free.text.sms.database.MmsDatabase;
import free.text.sms.database.ThreadDatabase;
import free.text.sms.database.model.MessageRecord;
import free.text.sms.jobs.MmsSendJob;
import free.text.sms.jobs.SmsSendJob;
import free.text.sms.mms.OutgoingMediaMessage;
import free.text.sms.recipients.Recipients;
import ws.com.google.android.mms.MmsException;

public class MessageSender {
    private static MasterSecret masterSecret;


    private static final String TAG = MessageSender.class.getSimpleName();

    public static long send(final Context context,
                            final MasterSecret masterSecret,
                            final OutgoingTextMessage message,
                            final long threadId,
                            final boolean forceSms) {
        EncryptingSmsDatabase database = DatabaseFactory.getEncryptingSmsDatabase(context);
        Recipients recipients = message.getRecipients();
        boolean keyExchange = message.isKeyExchange();

        long allocatedThreadId;

        if (threadId == -1) {
            allocatedThreadId = DatabaseFactory.getThreadDatabase(context).getThreadIdFor(recipients);
        } else {
            allocatedThreadId = threadId;
        }

        long messageId = database.insertMessageOutbox(masterSecret, allocatedThreadId,
                message, forceSms, System.currentTimeMillis());

        sendTextMessage(context, recipients, messageId);

        return allocatedThreadId;
    }

    public static long send(final Context context,
                            final MasterSecret masterSecret,
                            final OutgoingMediaMessage message,
                            final long threadId,
                            final boolean forceSms) {
        try {
            ThreadDatabase threadDatabase = DatabaseFactory.getThreadDatabase(context);
            MmsDatabase database = DatabaseFactory.getMmsDatabase(context);

            long allocatedThreadId;

            if (threadId == -1) {
                allocatedThreadId = threadDatabase.getThreadIdFor(message.getRecipients(), message.getDistributionType());
            } else {
                allocatedThreadId = threadId;
            }

            Recipients recipients = message.getRecipients();
            long messageId = database.insertMessageOutbox(masterSecret, message, allocatedThreadId, forceSms);

            sendMediaMessage(context, messageId);

            return allocatedThreadId;
        } catch (MmsException e) {
            Log.w(TAG, e);
            return threadId;
        }
    }

    public static void resend(Context context, MasterSecret masterSecret, MessageRecord messageRecord) {
        try {
            long messageId = messageRecord.getId();

            if (messageRecord.isMms()) {
                Recipients recipients = DatabaseFactory.getMmsAddressDatabase(context).getRecipientsForId(messageId);
                sendMediaMessage(context, messageId);
            } else {
                Recipients recipients = messageRecord.getRecipients();
                sendTextMessage(context, recipients, messageId);
            }
        } catch (MmsException e) {
            Log.w(TAG, e);
        }

    }

    private static void sendMediaMessage(Context context, long messageId)
            throws MmsException {
        JobManager jobManager = ApplicationContext.getInstance(context).getJobManager();
        jobManager.add(new MmsSendJob(context, messageId));
    }

    private static void sendTextMessage(Context context, Recipients recipients, long messageId) {
        JobManager jobManager = ApplicationContext.getInstance(context).getJobManager();
        jobManager.add(new SmsSendJob(context, messageId, recipients.getPrimaryRecipient().getName()));
    }
}
