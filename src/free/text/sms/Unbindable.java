package free.text.sms;

public interface Unbindable {
  public void unbind();
}
