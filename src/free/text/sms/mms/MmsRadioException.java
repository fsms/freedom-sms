package free.text.sms.mms;

public class MmsRadioException extends Throwable {
  public MmsRadioException(String s) {
    super(s);
  }
}
