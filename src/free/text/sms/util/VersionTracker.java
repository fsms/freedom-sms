package free.text.sms.util;

import android.content.Context;

import java.io.IOException;

public class VersionTracker {


  public static int getLastSeenVersion(Context context) {
    return SilencePreferences.getLastVersionCode(context);
  }

  public static void updateLastSeenVersion(Context context) {
    try {
      int currentVersionCode = Util.getCurrentApkReleaseVersion(context);
      SilencePreferences.setLastVersionCode(context, currentVersionCode);
    } catch (IOException ioe) {
      throw new AssertionError(ioe);
    }
  }
}
