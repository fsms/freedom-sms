package free.text.sms.util.concurrent;

import java.util.concurrent.ExecutionException;

import free.text.sms.util.concurrent.ListenableFuture.Listener;

public abstract class AssertedSuccessListener<T> implements Listener<T> {
  @Override
  public void onFailure(ExecutionException e) {
    throw new AssertionError(e);
  }
}
