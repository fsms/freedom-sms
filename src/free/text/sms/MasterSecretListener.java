package free.text.sms;

public interface MasterSecretListener {
  void onMasterSecretCleared();
}
