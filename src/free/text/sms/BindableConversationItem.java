package free.text.sms;

import android.support.annotation.NonNull;

import free.text.sms.crypto.MasterSecret;
import free.text.sms.database.model.MessageRecord;
import free.text.sms.recipients.Recipients;

import java.util.Locale;
import java.util.Set;

public interface BindableConversationItem extends Unbindable {
  void bind(@NonNull MasterSecret masterSecret,
            @NonNull MessageRecord messageRecord,
            @NonNull Locale locale,
            @NonNull Set<MessageRecord> batchSelected,
            @NonNull Recipients recipients);
}
