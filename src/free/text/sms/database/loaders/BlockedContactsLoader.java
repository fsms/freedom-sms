package free.text.sms.database.loaders;

import android.content.Context;
import android.database.Cursor;

import free.text.sms.database.DatabaseFactory;
import free.text.sms.util.AbstractCursorLoader;

public class BlockedContactsLoader extends AbstractCursorLoader {

  public BlockedContactsLoader(Context context) {
    super(context);
  }

  @Override
  public Cursor getCursor() {
    return DatabaseFactory.getRecipientPreferenceDatabase(getContext())
                          .getBlocked();
  }

}
