package free.text.sms.crypto;

import android.content.Context;
import android.support.annotation.NonNull;

import free.text.sms.crypto.storage.SilenceSessionStore;
import free.text.sms.recipients.Recipient;
import org.whispersystems.libaxolotl.AxolotlAddress;
import org.whispersystems.libaxolotl.state.SessionStore;

public class SessionUtil {

  public static boolean hasSession(Context context, MasterSecret masterSecret, Recipient recipient) {
    return hasSession(context, masterSecret, recipient.getNumber());
  }

  public static boolean hasSession(Context context, MasterSecret masterSecret, @NonNull String number) {
    SessionStore   sessionStore   = new SilenceSessionStore(context, masterSecret);
    AxolotlAddress axolotlAddress = new AxolotlAddress(number, 1);

    return sessionStore.containsSession(axolotlAddress);
  }
}
