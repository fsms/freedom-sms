package free.text.sms;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.whispersystems.libaxolotl.util.guava.Optional;

import java.util.LinkedList;
import java.util.List;

import free.text.sms.util.CharacterCalculator;
import free.text.sms.util.EncryptedSmsCharacterCalculator;
import free.text.sms.util.MmsCharacterCalculator;
import free.text.sms.util.SmsCharacterCalculator;
import free.text.sms.util.dualsim.SubscriptionInfoCompat;
import free.text.sms.util.dualsim.SubscriptionManagerCompat;

public class TransportOptions {

  private static final String TAG = TransportOptions.class.getSimpleName();

  private final List<OnTransportChangedListener> listeners = new LinkedList<>();
  private final Context context;
  private final List<TransportOption>            enabledTransports;

  private TransportOption.Type defaultTransportType  = TransportOption.Type.INSECURE_SMS;
  private Optional<Integer>         defaultSubscriptionId = SubscriptionManagerCompat.getDefaultMessagingSubscriptionId();
  private Optional<TransportOption> selectedOption        = Optional.absent();

  public TransportOptions(Context context, boolean media) {
    this.context           = context;
    this.enabledTransports = initializeAvailableTransports(media);
  }

  public void reset(boolean media) {
    List<TransportOption> transportOptions = initializeAvailableTransports(media);

    this.enabledTransports.clear();
    this.enabledTransports.addAll(transportOptions);

    if (selectedOption.isPresent() && !isEnabled(selectedOption.get())) {
      setSelectedTransport(null);
    } else {
      this.defaultTransportType = TransportOption.Type.INSECURE_SMS;
      this.defaultSubscriptionId = SubscriptionManagerCompat.getDefaultMessagingSubscriptionId();

      notifyTransportChangeListeners();
    }
  }

  public void setDefaultTransport(TransportOption.Type type) {
    this.defaultTransportType = type;

    if (!selectedOption.isPresent()) {
      notifyTransportChangeListeners();
    }
  }

  public void setDefaultSubscriptionId(Optional<Integer> subscriptionId) {
    if(subscriptionId.isPresent() && subscriptionId.get() >= 0) {
      this.defaultSubscriptionId = subscriptionId;
    }

    if (!selectedOption.isPresent()) {
      notifyTransportChangeListeners();
    }
  }

  public void setSelectedTransport(@Nullable TransportOption transportOption) {
    this.selectedOption = Optional.fromNullable(transportOption);
    notifyTransportChangeListeners();
  }

  public boolean isManualSelection() {
    return this.selectedOption.isPresent();
  }

  public @NonNull
  TransportOption getSelectedTransport() {
    if (selectedOption.isPresent()) return selectedOption.get();

    if (defaultSubscriptionId.isPresent()) {
      for (TransportOption transportOption : enabledTransports) {
        if (transportOption.getType() == defaultTransportType &&
            (int)defaultSubscriptionId.get() == transportOption.getSimSubscriptionId().or(-1))
        {
          return transportOption;
        }
      }
    }

    for (TransportOption transportOption : enabledTransports) {
      if (transportOption.getType() == defaultTransportType) {
        return transportOption;
      }
    }

    throw new AssertionError("No options of default type!");
  }

  public void disableTransport(TransportOption.Type type) {
    List<TransportOption> options = find(type);

    for (TransportOption option : options) {
      enabledTransports.remove(option);
      if (selectedOption.isPresent() && selectedOption.get().getType() == type) {
        setSelectedTransport(null);
      }
    }
  }

  public List<TransportOption> getEnabledTransports() {
    return enabledTransports;
  }

  public void addOnTransportChangedListener(OnTransportChangedListener listener) {
    this.listeners.add(listener);
  }

  private List<TransportOption> initializeAvailableTransports(boolean isMediaMessage) {
    List<TransportOption> results = new LinkedList<>();

    if (isMediaMessage) {
      results.addAll(getTransportOptionsForSimCards(TransportOption.Type.INSECURE_SMS, free.text.sms.R.drawable.ic_send_insecure_white_24dp,
                                                    context.getResources().getColor(free.text.sms.R.color.grey_600),
                                                    context.getString(free.text.sms.R.string.ConversationActivity_transport_insecure_mms),
                                                    context.getString(free.text.sms.R.string.conversation_activity__type_message_mms_insecure),
                                                    new MmsCharacterCalculator()));
      results.addAll(getTransportOptionsForSimCards(TransportOption.Type.SECURE_SMS, free.text.sms.R.drawable.ic_send_secure_white_24dp,
                                                    context.getResources().getColor(free.text.sms.R.color.silence_primary),
                                                    context.getString(free.text.sms.R.string.ConversationActivity_transport_secure_mms),
                                                    context.getString(free.text.sms.R.string.conversation_activity__type_message_mms_secure),
                                                    new MmsCharacterCalculator()));
    } else {
      results.addAll(getTransportOptionsForSimCards(TransportOption.Type.INSECURE_SMS, free.text.sms.R.drawable.ic_send_insecure_white_24dp,
                                                    context.getResources().getColor(free.text.sms.R.color.grey_600),
                                                    context.getString(free.text.sms.R.string.ConversationActivity_transport_insecure_sms),
                                                    context.getString(free.text.sms.R.string.conversation_activity__type_message_sms_insecure),
                                                    new SmsCharacterCalculator()));
      results.addAll(getTransportOptionsForSimCards(TransportOption.Type.SECURE_SMS, free.text.sms.R.drawable.ic_send_secure_white_24dp,
                                                    context.getResources().getColor(free.text.sms.R.color.silence_primary),
                                                    context.getString(free.text.sms.R.string.ConversationActivity_transport_secure_sms),
                                                    context.getString(free.text.sms.R.string.conversation_activity__type_message_sms_secure),
                                                    new EncryptedSmsCharacterCalculator()));
    }

    return results;
  }

  private @NonNull List<TransportOption> getTransportOptionsForSimCards(@NonNull TransportOption.Type type,
                                                                        @DrawableRes int drawable,
                                                                        int backgroundColor,
                                                                        @NonNull String text,
                                                                        @NonNull String composeHint,
                                                                        @NonNull CharacterCalculator characterCalculator)
  {
    List<TransportOption>        results             = new LinkedList<>();
    SubscriptionManagerCompat    subscriptionManager = new SubscriptionManagerCompat(context);
    List<SubscriptionInfoCompat> subscriptions       = subscriptionManager.getActiveSubscriptionInfoList();

    if (subscriptions.size() < 2) {
      results.add(new TransportOption(type,
                                      drawable,
                                      backgroundColor,
                                      text,
                                      composeHint,
                                      characterCalculator));
    } else {
      for (SubscriptionInfoCompat subscriptionInfo : subscriptions) {
        results.add(new TransportOption(type,
                                        drawable,
                                        backgroundColor,
                                        text,
                                        composeHint,
                                        characterCalculator,
                                        Optional.of(subscriptionInfo.getDisplayName()),
                                        Optional.of(subscriptionInfo.getSubscriptionId())));
      }
    }

    return results;
  }

  private void notifyTransportChangeListeners() {
    for (OnTransportChangedListener listener : listeners) {
      listener.onChange(getSelectedTransport(), selectedOption.isPresent());
    }
  }

  private List<TransportOption> find(TransportOption.Type type) {
    List<TransportOption> options = new LinkedList<>();
    for (TransportOption option : enabledTransports) {
      if (option.isType(type)) {
        options.add(option);
      }
    }
    return options;
  }

  private boolean isEnabled(TransportOption transportOption) {
    for (TransportOption option : enabledTransports) {
      if (option.equals(transportOption)) return true;
    }

    return false;
  }

  public interface OnTransportChangedListener {
    public void onChange(TransportOption newTransport, boolean manuallySelected);
  }
}
