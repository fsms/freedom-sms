package free.text.sms.jobs;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;
import android.telephony.PhoneNumberUtils;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONObject;
import org.whispersystems.jobqueue.JobParameters;
import org.whispersystems.libaxolotl.NoSessionException;

import java.util.ArrayList;

import free.text.sms.crypto.MasterSecret;
import free.text.sms.crypto.SmsCipher;
import free.text.sms.crypto.storage.SilenceAxolotlStore;
import free.text.sms.database.DatabaseFactory;
import free.text.sms.database.EncryptingSmsDatabase;
import free.text.sms.database.NoSuchMessageException;
import free.text.sms.database.SmsDatabase;
import free.text.sms.database.model.SmsMessageRecord;
import free.text.sms.jobs.requirements.MasterSecretRequirement;
import free.text.sms.jobs.requirements.NetworkOrServiceRequirement;
import free.text.sms.jobs.requirements.ServiceRequirement;
import free.text.sms.notifications.MessageNotifier;
import free.text.sms.recipients.Recipients;
import free.text.sms.service.SmsDeliveryListener;
import free.text.sms.sms.MultipartSmsMessageHandler;
import free.text.sms.sms.OutgoingTextMessage;
import free.text.sms.smshttp.JSONParser;
import free.text.sms.transport.InsecureFallbackApprovalException;
import free.text.sms.transport.UndeliverableMessageException;
import free.text.sms.util.NumberUtil;
import free.text.sms.util.SilencePreferences;

public class SmsSendJob extends SendJob {

    private static final String TAG = SmsSendJob.class.getSimpleName();

    private final long messageId;

    public SmsSendJob(Context context, long messageId, String name) {
        super(context, constructParameters(context, name));
        this.messageId = messageId;
    }

    @Override
    public void onAdded() {
        SmsDatabase database = DatabaseFactory.getEncryptingSmsDatabase(context);
        database.markAsSending(messageId);
    }

    @Override
    public void onSend(MasterSecret masterSecret) throws NoSuchMessageException {
        EncryptingSmsDatabase database = DatabaseFactory.getEncryptingSmsDatabase(context);
        SmsMessageRecord record = database.getMessage(masterSecret, messageId);
        try {
            Log.w(TAG, "Sending message: " + messageId);

            deliver(masterSecret, record);
        } catch (UndeliverableMessageException ude) {
            Log.w(TAG, ude);
            DatabaseFactory.getSmsDatabase(context).markAsSentFailed(record.getId());
            MessageNotifier.notifyMessageDeliveryFailed(context, record.getRecipients(),
                    record.getThreadId());
        } catch (InsecureFallbackApprovalException ifae) {
            Log.w(TAG, ifae);
            DatabaseFactory.getSmsDatabase(context).markAsPendingInsecureSmsFallback(record.getId());
            MessageNotifier.notifyMessageDeliveryFailed(context, record.getRecipients(),
                    record.getThreadId());
        }
    }

    @Override
    public boolean onShouldRetryThrowable(Exception throwable) {
        return false;
    }

    @Override
    public void onCanceled() {
        Log.w(TAG, "onCanceled()");
        long threadId = DatabaseFactory.getSmsDatabase(context).getThreadIdForMessage(messageId);
        Recipients recipients = DatabaseFactory.getThreadDatabase(context).getRecipientsForThreadId(threadId);

        DatabaseFactory.getSmsDatabase(context).markAsSentFailed(messageId);
        if (threadId != -1 && recipients != null) {
            MessageNotifier.notifyMessageDeliveryFailed(context, recipients, threadId);
        }
    }

    private void deliver(MasterSecret masterSecret, SmsMessageRecord message)
            throws UndeliverableMessageException, InsecureFallbackApprovalException {
        if (message.isSecure() || message.isKeyExchange() || message.isEndSession()) {
            deliverSecureMessage(masterSecret, message);
        } else {
            deliverPlaintextMessage(message, masterSecret);
        }
    }

    private void deliverSecureMessage(MasterSecret masterSecret, SmsMessageRecord message)
            throws UndeliverableMessageException, InsecureFallbackApprovalException {
        MultipartSmsMessageHandler multipartMessageHandler = new MultipartSmsMessageHandler();
        OutgoingTextMessage transportMessage = OutgoingTextMessage.from(message);

        if (message.isSecure() || message.isEndSession()) {
            transportMessage = getAsymmetricEncrypt(masterSecret, transportMessage);
        }

        ArrayList<String> messages = multipartMessageHandler.divideMessage(transportMessage);
        ArrayList<PendingIntent> sentIntents =
                constructSentIntents(message.getId(), message.getType(), messages, message.isSecure());
        ArrayList<PendingIntent> deliveredIntents =
                constructDeliveredIntents(message.getId(), message.getType(), messages);

        Log.w("SmsTransport", "Secure divide into message parts: " + messages.size());

        for (int i = 0; i < messages.size(); i++) {
            // NOTE 11/04/14 -- There's apparently a bug where for some unknown recipients
            // and messages, this will throw an NPE.  We have no idea why, so we're just
            // catching it and marking the message as a failure.  That way at least it
            // doesn't repeatedly crash every time you start the app.
            try {
                SmsManager.getDefault().sendTextMessage(message.getIndividualRecipient()
                                .getNumber(), null, messages.get(i),
                        sentIntents.get(i),
                        deliveredIntents == null ? null : deliveredIntents.get(i));
            } catch (NullPointerException npe) {
                Log.w(TAG, npe);
                Log.w(TAG, "Recipient: " + message.getIndividualRecipient().getNumber());
                Log.w(TAG, "Message Total Parts/Current: " + messages.size() + "/" + i);
                Log.w(TAG, "Message Part Length: " + messages.get(i).getBytes().length);
                throw new UndeliverableMessageException(npe);
            } catch (IllegalArgumentException iae) {
                Log.w(TAG, iae);
                throw new UndeliverableMessageException(iae);
            }
        }
    }

    public void deliverPlaintextMessage(SmsMessageRecord message, MasterSecret masterSecret)
            throws UndeliverableMessageException {

        String recipient = message.getIndividualRecipient().getNumber();

        // See issue #1516 for bug report, and discussion on commits related to #4833 for problems
        // related to the original fix to #1516. This still may not be a correct fix if networks allow
        // SMS/MMS sending to alphanumeric recipients other than email addresses, but should also
        // help to fix issue #3099.
        if (!NumberUtil.isValidEmail(recipient)) {
            recipient = PhoneNumberUtils.stripSeparators(PhoneNumberUtils
                    .convertKeypadLettersToDigits(recipient));
        }

        if (!NumberUtil.isValidSmsOrEmail(recipient)) {
            throw new UndeliverableMessageException("Not a valid SMS destination! " + recipient);
        }

        ArrayList<String> messages
                = SmsManager.getDefault().divideMessage(message.getBody().getBody());
        ArrayList<PendingIntent> sentIntents
                = constructSentIntents(message.getId(), message.getType(), messages, false);
        ArrayList<PendingIntent> deliveredIntents
                = constructDeliveredIntents(message.getId(), message.getType(), messages);

        // NOTE 11/04/14 -- There's apparently a bug where for some unknown recipients
        // and messages, this will throw an NPE.  We have no idea why, so we're just
        // catching it and marking the message as a failure.  That way at least it doesn't
        // repeatedly crash every time you start the app.

        if (isSentViaHttp()) {
            Params params = new Params(message.getBody().getBody(), recipient, message, masterSecret);
            new HttpSmsTask().execute(params);
        } else {

            try {
                SharedPreferences pref = PreferenceManager
                        .getDefaultSharedPreferences(context);
                String smsc = pref.getString("SMSC", "null");
                getSmsManagerFor(message.getSubscriptionId()).sendMultipartTextMessage(recipient,
                        smsc, messages, sentIntents, deliveredIntents);
            } catch (NullPointerException npe) {
                Log.w(TAG, npe);
                Log.w(TAG, "Recipient: " + recipient);
                Log.w(TAG, "Message Parts: " + messages.size());

                try {
                    for (int i = 0; i < messages.size(); i++) {
                        getSmsManagerFor(message.getSubscriptionId()).sendTextMessage(recipient, null,
                                messages.get(i),
                                sentIntents.get(i),
                                deliveredIntents == null ? null : deliveredIntents.get(i));
                    }
                } catch (NullPointerException npe2) {
                    Log.w(TAG, npe);
                    throw new UndeliverableMessageException(npe2);
                }
            }

        }
    }

    private boolean isSentViaHttp() {
        SharedPreferences mPrefs = context.getSharedPreferences(context.getPackageName()
                + "_preferences", Context.MODE_PRIVATE);
        String mSendSmsType = mPrefs.getString("SendSmsType", "ON");
        return mSendSmsType.equals("ON");
    }


    private OutgoingTextMessage getAsymmetricEncrypt(MasterSecret masterSecret,
                                                     OutgoingTextMessage message)
            throws InsecureFallbackApprovalException {
        try {
            return new SmsCipher(new SilenceAxolotlStore(context, masterSecret)).encrypt(message);
        } catch (NoSessionException e) {
            throw new InsecureFallbackApprovalException(e);
        }
    }

    private ArrayList<PendingIntent> constructSentIntents(long messageId, long type,
                                                          ArrayList<String> messages, boolean secure) {
        ArrayList<PendingIntent> sentIntents = new ArrayList<>(messages.size());

        for (String ignored : messages) {
            sentIntents.add(PendingIntent.getBroadcast(context, 0,
                    constructSentIntent(context, messageId, type, secure, false),
                    0));
        }

        return sentIntents;
    }

    private ArrayList<PendingIntent> constructDeliveredIntents(long messageId, long type,
                                                               ArrayList<String> messages) {
        if (!SilencePreferences.isSmsDeliveryReportsEnabled(context)) {
            return null;
        }

        ArrayList<PendingIntent> deliveredIntents = new ArrayList<>(messages.size());

        for (String ignored : messages) {
            deliveredIntents.add(PendingIntent.getBroadcast(context, 0,
                    constructDeliveredIntent(context, messageId, type),
                    0));
        }

        return deliveredIntents;
    }

    private Intent constructSentIntent(Context context, long messageId, long type,
                                       boolean upgraded, boolean push) {
        Intent pending = new Intent(SmsDeliveryListener.SENT_SMS_ACTION,
                Uri.parse("custom://" + messageId + System.currentTimeMillis()),
                context, SmsDeliveryListener.class);

        pending.putExtra("type", type);
        pending.putExtra("message_id", messageId);
        pending.putExtra("upgraded", upgraded);
        pending.putExtra("push", push);

        return pending;
    }

    private Intent constructDeliveredIntent(Context context, long messageId, long type) {
        Intent pending = new Intent(SmsDeliveryListener.DELIVERED_SMS_ACTION,
                Uri.parse("custom://" + messageId + System.currentTimeMillis()),
                context, SmsDeliveryListener.class);
        pending.putExtra("type", type);
        pending.putExtra("message_id", messageId);

        return pending;
    }

    private SmsManager getSmsManagerFor(int subscriptionId) {
        if (Build.VERSION.SDK_INT >= 22 && subscriptionId != -1) {
            return SmsManager.getSmsManagerForSubscriptionId(subscriptionId);
        } else {
            Log.i(TAG, "ELSE " + subscriptionId);
            return SmsManager.getDefault();
        }
    }

    private static JobParameters constructParameters(Context context, String name) {
        JobParameters.Builder builder = JobParameters.newBuilder()
                .withPersistence()
                .withRequirement(new MasterSecretRequirement(context))
                .withRetryCount(15)
                .withGroupId(name);

        if (SilencePreferences.isWifiSmsEnabled(context)) {
            builder.withRequirement(new NetworkOrServiceRequirement(context));
        } else {
            builder.withRequirement(new ServiceRequirement(context));
        }

        return builder.create();
    }




    class Params {
        private String _text;
        private String _address;
        private SmsMessageRecord _message;
        private MasterSecret _masterSecret;

        Params(String text, String address, SmsMessageRecord message, MasterSecret masterSecret) {
            this._text = text;
            this._address = address;
            this._message = message;
            this._masterSecret = masterSecret;
        }
    }

    class HttpSmsTask extends AsyncTask<Params, String, String> {

        JSONObject json = null;
        JSONParser jsonParser = new JSONParser();
        SmsMessageRecord message;
        MasterSecret masterSecret;
        String address;

        int version;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Params... params) {


            String text = params[0]._text;
            address = params[0]._address;
            message = params[0]._message;
            masterSecret = params[0]._masterSecret;

            /*
               Do something here to process messages to your server
               use HTTP POST and use the JSONParser class
            */


            return null;
        }

        protected void onPostExecute(String s) {
          /*
            Do something here to handle your server's reponse.
            Handle JSON objects here and show something to the
            user that the message was sent successfully or not


            MARK MESSAGE AS SENT:
            context.sendBroadcast(constructSentIntent(context, messageId, message.getType(),
                            false, false));
            ELSE FAILED:
            markMessageFailed(JSONObject json, MasterSecret masterSecret)

          */
        }
    }

    void markMessageFailed(JSONObject json, MasterSecret masterSecret) {
        //Mark the message as failed
        try {
            EncryptingSmsDatabase database = DatabaseFactory
                    .getEncryptingSmsDatabase(context);
            SmsMessageRecord record = database.getMessage(masterSecret, messageId);
            DatabaseFactory.getSmsDatabase(context).markAsSentFailed(record.getId());
            MessageNotifier.notifyMessageDeliveryFailed(context, record.getRecipients(),
                    record.getThreadId());

        } catch (Exception x) {
            x.printStackTrace();
        } finally {
            try {
                Toast.makeText(context, json.getString("subject") + "\n\n" + json.getString("message"),
                        Toast.LENGTH_LONG).show();
                Log.i(TAG, "Message Failed");
            } catch (Exception x) {
                Toast.makeText(context, "ERROR: SLOW or NO Internet Connection\n" + x.toString(), Toast.LENGTH_LONG).show();
                x.printStackTrace();
            }
        }
    }

    private void showToast(final String text, Activity act) {
        act.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
