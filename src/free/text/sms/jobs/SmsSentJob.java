package free.text.sms.jobs;

import android.app.Activity;
import android.content.Context;
import android.telephony.SmsManager;
import android.util.Log;

import org.whispersystems.jobqueue.JobParameters;
import org.whispersystems.libaxolotl.state.SessionStore;

import free.text.sms.ApplicationContext;
import free.text.sms.crypto.MasterSecret;
import free.text.sms.crypto.SecurityEvent;
import free.text.sms.crypto.storage.SilenceSessionStore;
import free.text.sms.database.DatabaseFactory;
import free.text.sms.database.EncryptingSmsDatabase;
import free.text.sms.database.NoSuchMessageException;
import free.text.sms.database.model.SmsMessageRecord;
import free.text.sms.jobs.requirements.MasterSecretRequirement;
import free.text.sms.notifications.MessageNotifier;
import free.text.sms.service.SmsDeliveryListener;
import free.text.sms.util.SilencePreferences;

public class SmsSentJob extends MasterSecretJob {

  private static final String TAG = SmsSentJob.class.getSimpleName();

  private final long   messageId;
  private final String action;
  private final int    result;

  public SmsSentJob(Context context, long messageId, String action, int result) {
    super(context, JobParameters.newBuilder()
                                .withPersistence()
                                .withRequirement(new MasterSecretRequirement(context))
                                .create());

    this.messageId = messageId;
    this.action    = action;
    this.result    = result;
  }

  @Override
  public void onAdded() {

  }

  @Override
  public void onRun(MasterSecret masterSecret) {
    Log.w(TAG, "Got SMS callback: " + action + " , " + result);

    switch (action) {
      case SmsDeliveryListener.SENT_SMS_ACTION:
        handleSentResult(masterSecret, messageId, result);
        break;
      case SmsDeliveryListener.DELIVERED_SMS_ACTION:
        handleDeliveredResult(masterSecret, messageId, result);
        break;
    }
  }

  @Override
  public boolean onShouldRetryThrowable(Exception throwable) {
    return false;
  }

  @Override
  public void onCanceled() {

  }

  private void handleDeliveredResult(MasterSecret masterSecret, long messageId, int result) {
    try {
      EncryptingSmsDatabase database      = DatabaseFactory.getEncryptingSmsDatabase(context);
      SmsMessageRecord record        = database.getMessage(masterSecret, messageId);
      String                recipientName = (record.getIndividualRecipient().getName() == null ? record.getIndividualRecipient().getNumber() : record.getIndividualRecipient().getName());

      if (!record.isDelivered() && SilencePreferences.isSmsDeliveryReportsToastEnabled(context)){
        MessageNotifier.sendDeliveryToast(context, recipientName);
      }
      DatabaseFactory.getEncryptingSmsDatabase(context).markAsReceived(messageId);
    } catch (NoSuchMessageException e) {
      Log.w(TAG, e);
    }
  }

  private void handleSentResult(MasterSecret masterSecret, long messageId, int result) {
    try {
      EncryptingSmsDatabase database = DatabaseFactory.getEncryptingSmsDatabase(context);
      SmsMessageRecord      record   = database.getMessage(masterSecret, messageId);

      switch (result) {
        case Activity.RESULT_OK:
          database.markAsSent(messageId);

          if (record != null && record.isEndSession()) {
            Log.w(TAG, "Ending session...");
            SessionStore sessionStore = new SilenceSessionStore(context, masterSecret);
            sessionStore.deleteAllSessions(record.getIndividualRecipient().getNumber());
            SecurityEvent.broadcastSecurityUpdateEvent(context, record.getThreadId());
          }

          break;
        case SmsManager.RESULT_ERROR_NO_SERVICE:
        case SmsManager.RESULT_ERROR_RADIO_OFF:
          Log.w(TAG, "Service connectivity problem, requeuing...");
          ApplicationContext.getInstance(context)
              .getJobManager()
              .add(new SmsSendJob(context, messageId, record.getIndividualRecipient().getNumber()));

          break;
        default:
          database.markAsSentFailed(messageId);
          MessageNotifier.notifyMessageDeliveryFailed(context, record.getRecipients(), record.getThreadId());
      }
    } catch (NoSuchMessageException e) {
      Log.w(TAG, e);
    }
  }
}
