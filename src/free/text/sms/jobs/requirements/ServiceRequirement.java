package free.text.sms.jobs.requirements;

import android.content.Context;

import org.whispersystems.jobqueue.dependencies.ContextDependent;
import org.whispersystems.jobqueue.requirements.Requirement;

import free.text.sms.sms.TelephonyServiceState;

public class ServiceRequirement implements Requirement, ContextDependent {

  private static final String TAG = ServiceRequirement.class.getSimpleName();

  private transient Context context;

  public ServiceRequirement(Context context) {
    this.context  = context;
  }

  @Override
  public void setContext(Context context) {
    this.context = context;
  }

  @Override
  public boolean isPresent() {
    TelephonyServiceState telephonyServiceState = new TelephonyServiceState();
    /*
      Force this to true to send even SIM is not present
      SMS status will be determined in SmsSendJob class
     */
    return true;//telephonyServiceState.isConnected(context);
  }
}
