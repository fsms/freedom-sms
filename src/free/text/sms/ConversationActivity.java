/**
 * Copyright (C) 2011 Whisper Systems
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package free.text.sms;

import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.view.WindowCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnKeyListener;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.pollfish.constants.Position;
import com.pollfish.interfaces.PollfishOpenedListener;
import com.pollfish.interfaces.PollfishSurveyCompletedListener;
import com.pollfish.interfaces.PollfishSurveyNotAvailableListener;
import com.pollfish.interfaces.PollfishSurveyReceivedListener;
import com.pollfish.main.PollFish;

import org.whispersystems.libaxolotl.InvalidMessageException;
import org.whispersystems.libaxolotl.util.guava.Optional;

import java.io.IOException;
import java.util.List;

import free.text.sms.TransportOptions.OnTransportChangedListener;
import free.text.sms.audio.AudioSlidePlayer;
import free.text.sms.color.MaterialColor;
import free.text.sms.components.AnimatingToggle;
import free.text.sms.components.ComposeText;
import free.text.sms.components.InputAwareLayout;
import free.text.sms.components.KeyboardAwareLinearLayout.OnKeyboardShownListener;
import free.text.sms.components.SendButton;
import free.text.sms.components.emoji.EmojiDrawer;
import free.text.sms.components.emoji.EmojiToggle;
import free.text.sms.contacts.ContactAccessor;
import free.text.sms.contacts.ContactAccessor.ContactData;
import free.text.sms.crypto.MasterCipher;
import free.text.sms.crypto.MasterSecret;
import free.text.sms.crypto.SecurityEvent;
import free.text.sms.crypto.SessionUtil;
import free.text.sms.database.DatabaseFactory;
import free.text.sms.database.DraftDatabase;
import free.text.sms.database.DraftDatabase.Draft;
import free.text.sms.database.DraftDatabase.Drafts;
import free.text.sms.database.GroupDatabase;
import free.text.sms.database.MmsSmsColumns.Types;
import free.text.sms.database.RecipientPreferenceDatabase;
import free.text.sms.database.ThreadDatabase;
import free.text.sms.mms.AttachmentManager;
import free.text.sms.mms.AttachmentManager.MediaType;
import free.text.sms.mms.AttachmentTypeSelectorAdapter;
import free.text.sms.mms.MediaConstraints;
import free.text.sms.mms.OutgoingMediaMessage;
import free.text.sms.mms.OutgoingSecureMediaMessage;
import free.text.sms.mms.Slide;
import free.text.sms.notifications.MessageNotifier;
import free.text.sms.protocol.AutoInitiate;
import free.text.sms.recipients.Recipient;
import free.text.sms.recipients.RecipientFactory;
import free.text.sms.recipients.RecipientFormattingException;
import free.text.sms.recipients.Recipients;
import free.text.sms.service.KeyCachingService;
import free.text.sms.sms.MessageSender;
import free.text.sms.sms.OutgoingEncryptedMessage;
import free.text.sms.sms.OutgoingEndSessionMessage;
import free.text.sms.sms.OutgoingTextMessage;
import free.text.sms.util.CharacterCalculator.CharacterState;
import free.text.sms.util.Dialogs;
import free.text.sms.util.DynamicLanguage;
import free.text.sms.util.DynamicTheme;
import free.text.sms.util.GroupUtil;
import free.text.sms.util.MediaUtil;
import free.text.sms.util.SilencePreferences;
import free.text.sms.util.Util;
import free.text.sms.util.ViewUtil;
import free.text.sms.util.concurrent.ListenableFuture;
import free.text.sms.util.concurrent.SettableFuture;
import free.text.sms.util.dualsim.SubscriptionManagerCompat;

/**
 * Activity for displaying a message thread, as well as
 * composing/sending a new message into that thread.
 *
 * @author Moxie Marlinspike
 */
public class ConversationActivity extends PassphraseRequiredActionBarActivity
        implements ConversationFragment.ConversationFragmentListener,
        AttachmentManager.AttachmentListener,
        Recipients.RecipientsModifiedListener,
        OnKeyboardShownListener {
    private static final String TAG = ConversationActivity.class.getSimpleName();

    public static final String RECIPIENTS_EXTRA = "recipients";
    public static final String THREAD_ID_EXTRA = "thread_id";
    public static final String IS_ARCHIVED_EXTRA = "is_archived";
    public static final String TEXT_EXTRA = "draft_text";
    public static final String DISTRIBUTION_TYPE_EXTRA = "distribution_type";

    private static final int PICK_IMAGE = 1;
    private static final int PICK_VIDEO = 2;
    private static final int PICK_AUDIO = 3;
    private static final int PICK_CONTACT_INFO = 4;
    private static final int GROUP_EDIT = 5;
    private static final int TAKE_PHOTO = 6;
    private static final int ADD_CONTACT = 7;

    private MasterSecret masterSecret;
    protected ComposeText composeText;
    private AnimatingToggle buttonToggle;
    private SendButton sendButton;
    private SendButton sendButton_locked;
    private ImageButton attachButton;
    protected ConversationTitleView titleView;
    private TextView charactersLeft;
    private ConversationFragment fragment;
    private Button unblockButton;
    private InputAwareLayout container;
    private View composePanel;
    private View composeBubble;

    private AttachmentTypeSelectorAdapter attachmentAdapter;
    private AttachmentManager attachmentManager;
    private BroadcastReceiver securityUpdateReceiver;
    private BroadcastReceiver groupUpdateReceiver;
    private EmojiDrawer emojiDrawer;
    private EmojiToggle emojiToggle;

    private Recipients recipients;
    private long threadId;
    private int distributionType;
    private boolean isEncryptedConversation;
    private boolean isSecureSmsDestination;
    private boolean archived;
    private boolean isMmsEnabled = true;

    private DynamicTheme dynamicTheme = new DynamicTheme();
    private DynamicLanguage dynamicLanguage = new DynamicLanguage();

    //Romel
    private String mSendSmsType;
    private SharedPreferences mPrefs;
    InterstitialAd mInterstitialAd;
    private InterstitialAd interstitialAd;

    @Override
    protected void onPreCreate() {
        dynamicTheme.onCreate(this);
        dynamicLanguage.onCreate(this);
    }

    @Override
    protected void onCreate(Bundle state, @NonNull MasterSecret masterSecret) {
        Log.w(TAG, "onCreate()");
        this.masterSecret = masterSecret;

        supportRequestWindowFeature(WindowCompat.FEATURE_ACTION_BAR_OVERLAY);
        setContentView(free.text.sms.R.layout.conversation_activity);

        fragment = initFragment(free.text.sms.R.id.fragment_content, new ConversationFragment(),
                masterSecret, dynamicLanguage.getCurrentLocale());

        initializeReceivers();
        initializeActionBar();
        initializeViews();
        initializeResources();
        initializeSecurity();
        updateRecipientPreferences();
        initializeDraft();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Log.w(TAG, "onNewIntent()");

        if (isFinishing()) {
            Log.w(TAG, "Activity is finishing...");
            return;
        }

        if (!Util.isEmpty(composeText) || attachmentManager.isAttachmentPresent()) {
            saveDraft();
            attachmentManager.clear();
            composeText.setText("");
        }

        setIntent(intent);
        initializeResources();
        initializeSecurity();
        updateRecipientPreferences();
        initializeDraft();

        if (fragment != null) {
            fragment.onNewIntent();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        dynamicTheme.onResume(this);
        dynamicLanguage.onResume(this);

        initializeEnabledCheck();
        initializeMmsEnabledCheck();
        composeText.setTransport(sendButton.getSelectedTransport());

        titleView.setTitle(recipients);
        setActionBarColor(recipients.getColor());
        setBlockedUserState(recipients);
        calculateCharactersRemaining();

        MessageNotifier.setVisibleThread(threadId);
        markThreadAsRead();

        pollfish();
    }


    @Override
    protected void onPause() {
        super.onPause();
        MessageNotifier.setVisibleThread(-1L);
        if (isFinishing())
            overridePendingTransition(free.text.sms.R.anim.fade_scale_in, free.text.sms.R.anim.slide_to_right);
        AudioSlidePlayer.stopAll();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        Log.w(TAG, "onConfigurationChanged(" + newConfig.orientation + ")");
        super.onConfigurationChanged(newConfig);
        composeText.setTransport(sendButton.getSelectedTransport());
        if (container.getCurrentInput() == emojiDrawer) container.hideAttachedInput(true);
    }

    @Override
    protected void onDestroy() {
        saveDraft();
        if (recipients != null) recipients.removeListener(this);
        if (securityUpdateReceiver != null) unregisterReceiver(securityUpdateReceiver);
        if (groupUpdateReceiver != null) unregisterReceiver(groupUpdateReceiver);
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        Log.w(TAG, "onActivityResult called: " + reqCode + ", " + resultCode + " , " + data);
        super.onActivityResult(reqCode, resultCode, data);

        if (data == null && reqCode != TAKE_PHOTO || resultCode != RESULT_OK) return;

        switch (reqCode) {
            case PICK_IMAGE:
                boolean isGif = MediaUtil.isGif(MediaUtil.getMimeType(this, data.getData()));
                setMedia(data.getData(), isGif ? MediaType.GIF : MediaType.IMAGE);
                break;
            case PICK_VIDEO:
                setMedia(data.getData(), MediaType.VIDEO);
                break;
            case PICK_AUDIO:
                setMedia(data.getData(), MediaType.AUDIO);
                break;
            case PICK_CONTACT_INFO:
                addAttachmentContactInfo(data.getData());
                break;
            case TAKE_PHOTO:
                if (attachmentManager.getCaptureUri() != null) {
                    setMedia(attachmentManager.getCaptureUri(), MediaType.IMAGE);
                }
                break;
            case ADD_CONTACT:
                recipients = RecipientFactory.getRecipientsForIds(ConversationActivity.this, recipients.getIds(), true);
                recipients.addListener(this);
                fragment.reloadList();
                break;
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuInflater inflater = this.getMenuInflater();
        menu.clear();

/*        if (isSingleConversation() && isEncryptedConversation) {
            inflater.inflate(free.text.sms.R.menu.conversation_secure_identity, menu);
            inflater.inflate(free.text.sms.R.menu.conversation_secure_sms, menu.findItem(free.text.sms.R.id.menu_security).getSubMenu());
        } else if (isSingleConversation()) {
            inflater.inflate(free.text.sms.R.menu.conversation_insecure_no_push, menu);
            inflater.inflate(free.text.sms.R.menu.conversation_insecure, menu);
        }*/

        if (isSingleConversation()) {
            inflater.inflate(free.text.sms.R.menu.conversation_callable, menu);
        } else if (isGroupConversation()) {
            inflater.inflate(free.text.sms.R.menu.conversation_group_options, menu);

            if (!isPushGroupConversation()) {
                inflater.inflate(free.text.sms.R.menu.conversation_mms_group_options, menu);
                if (distributionType == ThreadDatabase.DistributionTypes.BROADCAST) {
                    menu.findItem(free.text.sms.R.id.menu_distribution_broadcast).setChecked(true);
                } else {
                    menu.findItem(free.text.sms.R.id.menu_distribution_conversation).setChecked(true);
                }
            }
        }

        inflater.inflate(free.text.sms.R.menu.conversation, menu);

        if (recipients != null && recipients.isMuted())
            inflater.inflate(free.text.sms.R.menu.conversation_muted, menu);
        else inflater.inflate(free.text.sms.R.menu.conversation_unmuted, menu);

        if (isSingleConversation() && getRecipients().getPrimaryRecipient().getContactUri() == null) {
            inflater.inflate(free.text.sms.R.menu.conversation_add_to_contacts, menu);
        }

        if (archived) menu.findItem(free.text.sms.R.id.menu_archive_conversation)
                .setTitle(free.text.sms.R.string.conversation__menu_unarchive_conversation);

        //Romel

        if (isSentViaHttp()) {
            menu.findItem(R.id.menu_sms_conversation).setIcon(R.drawable.free);
        } else {
            menu.findItem(R.id.menu_sms_conversation).setIcon(R.drawable.sim);
        }

        super.onPrepareOptionsMenu(menu);
        return true;
    }

    private boolean isSentViaHttp() {
        mPrefs = getSharedPreferences(getPackageName() + "_preferences", Context.MODE_PRIVATE);
        mSendSmsType = mPrefs.getString("SendSmsType", "ON");
        return mSendSmsType.equals("ON");
    }


    private void switchSendSmsType() {
        final Context context = getApplicationContext();
        SharedPreferences.Editor editor = mPrefs.edit();
        if ("ON".equals(mSendSmsType)) {
            editor.putString("SendSmsType", "OFF");
            mSendSmsType = "OFF";
            warning();
        } else {
            editor.putString("SendSmsType", "ON");
            Toast.makeText(context, R.string.freetext_on, Toast.LENGTH_SHORT).show();
        }
        editor.apply();
    }

    void warning() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ConversationActivity.this);
        alertDialogBuilder.setMessage(R.string.menu_via_sim_warning);

        alertDialogBuilder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setTitle(R.string.warning_title);
        alertDialog.setIcon(R.drawable.ic_warning_black_36dp);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case free.text.sms.R.id.menu_call:
                handleDial(getRecipients().getPrimaryRecipient());
                return true;
            case free.text.sms.R.id.menu_delete_conversation:
                handleDeleteConversation();
                return true;
            case free.text.sms.R.id.menu_archive_conversation:
                handleArchiveConversation();
                return true;
           /* case free.text.sms.R.id.menu_add_attachment:
                handleAddAttachment();
                return true;
            case free.text.sms.R.id.menu_view_media:
                handleViewMedia();
                return true;*/
            case free.text.sms.R.id.menu_add_to_contacts:
                handleAddToContacts();
                return true;
/*            case free.text.sms.R.id.menu_start_secure_session:
                handleStartSecureSession();
                return true;
            case free.text.sms.R.id.menu_abort_session:
                handleAbortSecureSession();
                return true;
            case free.text.sms.R.id.menu_verify_identity:
                handleVerifyIdentity();
                return true;
            case free.text.sms.R.id.menu_group_recipients:
                handleDisplayGroupRecipients();
                return true;
            case free.text.sms.R.id.menu_distribution_broadcast:
                handleDistributionBroadcastEnabled(item);
                return true;
            case free.text.sms.R.id.menu_distribution_conversation:
                handleDistributionConversationEnabled(item);
                return true; */
            case free.text.sms.R.id.menu_invite:
                handleInviteLink();
                return true;
            case free.text.sms.R.id.menu_mute_notifications:
                handleMuteNotifications();
                return true;
            case free.text.sms.R.id.menu_unmute_notifications:
                handleUnmuteNotifications();
                return true;
            case free.text.sms.R.id.menu_conversation_settings:
                handleConversationSettings();
                return true;
            case android.R.id.home:
                handleReturnToConversationList();
                return true;
            //Romel
            case R.id.menu_sms_conversation:
                switchSendSmsType();
                invalidateOptionsMenu();
                return true;

        }

        return false;
    }

    @Override
    public void onBackPressed() {
        Log.w(TAG, "onBackPressed()");
        if (container.isInputOpen()) container.hideCurrentInput(composeText);
        else super.onBackPressed();
    }

    @Override
    public void onKeyboardShown() {
        emojiToggle.setToEmoji();
    }

    //////// Event Handlers

    private void handleReturnToConversationList() {
        Intent intent = new Intent(this, (archived ? ConversationListArchiveActivity.class : ConversationListActivity.class));
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    private void handleMuteNotifications() {
        MuteDialog.show(this, new MuteDialog.MuteSelectionListener() {
            @Override
            public void onMuted(final long until) {
                recipients.setMuted(until);

                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... params) {
                        DatabaseFactory.getRecipientPreferenceDatabase(ConversationActivity.this)
                                .setMuted(recipients, until);

                        return null;
                    }
                }.execute();
            }
        });
    }

    private void handleConversationSettings() {
        titleView.performClick();
    }

    private void handleUnmuteNotifications() {
        recipients.setMuted(0);

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                DatabaseFactory.getRecipientPreferenceDatabase(ConversationActivity.this)
                        .setMuted(recipients, 0);

                return null;
            }
        }.execute();
    }

    private void handleUnblock() {
        new AlertDialog.Builder(this)
                .setTitle(free.text.sms.R.string.RecipientPreferenceActivity_unblock_this_contact_question)
                .setMessage(free.text.sms.R.string.RecipientPreferenceActivity_are_you_sure_you_want_to_unblock_this_contact)
                .setNegativeButton(android.R.string.cancel, null)
                .setPositiveButton(free.text.sms.R.string.RecipientPreferenceActivity_unblock, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        recipients.setBlocked(false);

                        new AsyncTask<Void, Void, Void>() {
                            @Override
                            protected Void doInBackground(Void... params) {
                                DatabaseFactory.getRecipientPreferenceDatabase(ConversationActivity.this)
                                        .setBlocked(recipients, false);
                                return null;
                            }
                        }.execute();
                    }
                }).show();
    }

    private void handleInviteLink() {
        composeText.appendInvite(getString(free.text.sms.R.string.ConversationActivity_install_smssecure, "https://goo.gl/jLJeVH"));
    }

/*    private void handleVerifyIdentity() {
        Intent verifyIdentityIntent = new Intent(this, VerifyIdentityActivity.class);
        verifyIdentityIntent.putExtra("recipient", getRecipients().getPrimaryRecipient().getRecipientId());
        startActivity(verifyIdentityIntent);
    }

    private void handleStartSecureSession() {
        if (getRecipients() == null) {
            Toast.makeText(this, getString(free.text.sms.R.string.ConversationActivity_invalid_recipient),
                    Toast.LENGTH_LONG).show();
            return;
        }

        if (TelephonyUtil.isMyPhoneNumber(this, recipients.getPrimaryRecipient().getNumber())) {
            Toast.makeText(this, getString(free.text.sms.R.string.ConversationActivity_recipient_self),
                    Toast.LENGTH_LONG).show();
            return;
        }

        final Recipients recipients = getRecipients();
        final Recipient recipient = recipients.getPrimaryRecipient();
        final int subscriptionId = sendButton.getSelectedTransport().getSimSubscriptionId().or(-1);
        String recipientName = (recipient.getName() == null ? recipient.getNumber() : recipient.getName());

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(free.text.sms.R.string.ConversationActivity_initiate_secure_session_question);
        builder.setIconAttribute(free.text.sms.R.attr.dialog_info_icon);
        builder.setCancelable(true);
        builder.setMessage(String.format(getString(free.text.sms.R.string.ConversationActivity_initiate_secure_session_with_s_question),
                recipientName));
        builder.setPositiveButton(free.text.sms.R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (!isEncryptedConversation) {
                    KeyExchangeInitiator.initiate(ConversationActivity.this, masterSecret, recipients, true, subscriptionId);
                }
                long allocatedThreadId;
                if (threadId == -1) {
                    allocatedThreadId = DatabaseFactory.getThreadDatabase(getApplicationContext()).getThreadIdFor(recipients);
                } else {
                    allocatedThreadId = threadId;
                }
                Log.w(TAG, "Refreshing thread " + allocatedThreadId + "...");
                sendComplete(allocatedThreadId);
            }
        });

        builder.setNegativeButton(free.text.sms.R.string.no, null);
        builder.show();
    }*/

    private void handleAbortSecureSession() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(free.text.sms.R.string.ConversationActivity_abort_secure_session_confirmation);
        builder.setIconAttribute(free.text.sms.R.attr.dialog_alert_icon);
        builder.setCancelable(true);
        builder.setMessage(free.text.sms.R.string.ConversationActivity_are_you_sure_that_you_want_to_abort_this_secure_session_question);
        builder.setPositiveButton(free.text.sms.R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (isSingleConversation() && isEncryptedConversation) {
                    final Context context = getApplicationContext();

                    OutgoingEndSessionMessage endSessionMessage =
                            new OutgoingEndSessionMessage(new OutgoingTextMessage(getRecipients(), "TERMINATE", -1));

                    new AsyncTask<OutgoingEndSessionMessage, Void, Long>() {
                        @Override
                        protected Long doInBackground(OutgoingEndSessionMessage... messages) {
                            return MessageSender.send(context, masterSecret, messages[0], threadId, false);
                        }

                        @Override
                        protected void onPostExecute(Long result) {
                            sendComplete(result);
                        }
                    }.execute(endSessionMessage);
                }
            }
        });
        builder.setNegativeButton(free.text.sms.R.string.no, null);
        builder.show();
    }

    private void handleViewMedia() {
        Intent intent = new Intent(this, MediaOverviewActivity.class);
        intent.putExtra(MediaOverviewActivity.THREAD_ID_EXTRA, threadId);
        intent.putExtra(MediaOverviewActivity.RECIPIENT_EXTRA, recipients.getPrimaryRecipient().getRecipientId());
        startActivity(intent);
    }

    private void handleDistributionBroadcastEnabled(MenuItem item) {
        distributionType = ThreadDatabase.DistributionTypes.BROADCAST;
        item.setChecked(true);

        if (threadId != -1) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    DatabaseFactory.getThreadDatabase(ConversationActivity.this)
                            .setDistributionType(threadId, ThreadDatabase.DistributionTypes.BROADCAST);
                    return null;
                }
            }.execute();
        }
    }

    private void handleDistributionConversationEnabled(MenuItem item) {
        distributionType = ThreadDatabase.DistributionTypes.CONVERSATION;
        item.setChecked(true);

        if (threadId != -1) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    DatabaseFactory.getThreadDatabase(ConversationActivity.this)
                            .setDistributionType(threadId, ThreadDatabase.DistributionTypes.CONVERSATION);
                    return null;
                }
            }.execute();
        }
    }

    private void handleDial(Recipient recipient) {
        try {
            if (recipient == null) return;

            Intent dialIntent = new Intent(Intent.ACTION_DIAL,
                    Uri.parse("tel:" + recipient.getNumber()));
            startActivity(dialIntent);
        } catch (ActivityNotFoundException anfe) {
            Log.w(TAG, anfe);
            Dialogs.showAlertDialog(this,
                    getString(free.text.sms.R.string.ConversationActivity_calls_not_supported),
                    getString(free.text.sms.R.string.ConversationActivity_this_device_does_not_appear_to_support_dial_actions));
        }
    }

    private void handleDisplayGroupRecipients() {
        new GroupMembersDialog(this, getRecipients()).display();
    }

    private void handleDeleteConversation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(free.text.sms.R.string.ConversationActivity_delete_thread_question);
        builder.setIconAttribute(free.text.sms.R.attr.dialog_alert_icon);
        builder.setCancelable(true);
        builder.setMessage(free.text.sms.R.string.ConversationActivity_this_will_permanently_delete_all_messages_in_this_conversation);
        builder.setPositiveButton(free.text.sms.R.string.delete, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (threadId > 0) {
                    DatabaseFactory.getThreadDatabase(ConversationActivity.this).deleteConversation(threadId);
                }
                composeText.getText().clear();
                threadId = -1;
                finish();
            }
        });

        builder.setNegativeButton(android.R.string.cancel, null);
        builder.show();
    }

    private void handleArchiveConversation() {
        if (threadId > 0) {
            if (!archived)
                DatabaseFactory.getThreadDatabase(ConversationActivity.this).archiveConversation(threadId);
            else
                DatabaseFactory.getThreadDatabase(ConversationActivity.this).unarchiveConversation(threadId);
        }
        composeText.getText().clear();
        threadId = -1;
        finish();
    }

    private void handleAddToContacts() {
        try {
            final Intent intent = new Intent(Intent.ACTION_INSERT_OR_EDIT);
            intent.putExtra(ContactsContract.Intents.Insert.PHONE, recipients.getPrimaryRecipient().getNumber());
            intent.setType(ContactsContract.Contacts.CONTENT_ITEM_TYPE);
            startActivityForResult(intent, ADD_CONTACT);
        } catch (ActivityNotFoundException e) {
            Log.w(TAG, e);
        }
    }

    private void handleAddAttachment() {
        if (this.isMmsEnabled) {
            new AlertDialog.Builder(this).setAdapter(attachmentAdapter, new AttachmentTypeListener())
                    .show();
        } else {
            handleManualMmsRequired();
        }
    }

    private void handleManualMmsRequired() {
        Toast.makeText(this, free.text.sms.R.string.MmsDownloader_error_reading_mms_settings, Toast.LENGTH_LONG).show();

        Intent intent = new Intent(this, PromptMmsActivity.class);
        intent.putExtras(getIntent().getExtras());
        startActivity(intent);
    }

    ///// Initializers

    private void initializeDraft() {
        final String draftText = getIntent().getStringExtra(TEXT_EXTRA);
        final Uri draftMedia = getIntent().getData();
        final MediaType draftMediaType = MediaType.from(getIntent().getType());

        if (draftText != null) composeText.setText(draftText);
        if (draftMedia != null && draftMediaType != null) setMedia(draftMedia, draftMediaType);

        if (draftText == null && draftMedia == null && draftMediaType == null) {
            initializeDraftFromDatabase();
        } else {
            updateToggleButtonState();
        }
    }

    private void initializeEnabledCheck() {
        boolean enabled = !(isPushGroupConversation() && !isActiveGroup());
        composeText.setEnabled(enabled);
        sendButton.setEnabled(enabled);
    }

    private void initializeDraftFromDatabase() {
        new AsyncTask<Void, Void, List<Draft>>() {
            @Override
            protected List<Draft> doInBackground(Void... params) {
                MasterCipher masterCipher = new MasterCipher(masterSecret);
                DraftDatabase draftDatabase = DatabaseFactory.getDraftDatabase(ConversationActivity.this);
                List<Draft> results = draftDatabase.getDrafts(masterCipher, threadId);

                draftDatabase.clearDrafts(threadId);

                return results;
            }

            @Override
            protected void onPostExecute(List<Draft> drafts) {
                for (Draft draft : drafts) {
                    if (draft.getType().equals(Draft.TEXT)) {
                        composeText.setText(draft.getValue());
                    } else if (draft.getType().equals(Draft.IMAGE)) {
                        setMedia(Uri.parse(draft.getValue()), MediaType.IMAGE);
                    } else if (draft.getType().equals(Draft.AUDIO)) {
                        setMedia(Uri.parse(draft.getValue()), MediaType.AUDIO);
                    } else if (draft.getType().equals(Draft.VIDEO)) {
                        setMedia(Uri.parse(draft.getValue()), MediaType.VIDEO);
                    }
                }

                updateToggleButtonState();
            }
        }.execute();
    }

    private void initializeSecurity() {
        Recipient primaryRecipient = getRecipients() == null ? null : getRecipients().getPrimaryRecipient();
        boolean isMediaMessage = !recipients.isSingleRecipient() || attachmentManager.isAttachmentPresent();

        isSecureSmsDestination = isSingleConversation() && SessionUtil.hasSession(this, masterSecret, primaryRecipient);

        if (isSecureSmsDestination) {
            this.isEncryptedConversation = true;
        } else {
            this.isEncryptedConversation = false;
        }

        sendButton.resetAvailableTransports(isMediaMessage);
        if (!isSecureSmsDestination) sendButton.disableTransport(TransportOption.Type.SECURE_SMS);
        if (recipients.isGroupRecipient())
            sendButton.disableTransport(TransportOption.Type.INSECURE_SMS);

        if (isSecureSmsDestination) {
            sendButton.setDefaultTransport(TransportOption.Type.SECURE_SMS);
        } else {
            sendButton.setDefaultTransport(TransportOption.Type.INSECURE_SMS);
        }

        calculateCharactersRemaining();
        supportInvalidateOptionsMenu();
    }

    private void updateRecipientPreferences() {
        if (recipients.getPrimaryRecipient() != null &&
                recipients.getPrimaryRecipient().getContactUri() != null) {
            new RecipientPreferencesTask().execute(recipients);
        }
    }

    private void updateDefaultSubscriptionId(Optional<Integer> defaultSubscriptionId) {
        Log.w(TAG, "updateDefaultSubscriptionId(" + defaultSubscriptionId.orNull() + ")");
        sendButton.setDefaultSubscriptionId(defaultSubscriptionId);
    }

    private void initializeMmsEnabledCheck() {
        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... params) {
                return Util.isMmsCapable(ConversationActivity.this);
            }

            @Override
            protected void onPostExecute(Boolean isMmsEnabled) {
                ConversationActivity.this.isMmsEnabled = isMmsEnabled;
            }
        }.execute();
    }

    private void initializeViews() {
        titleView = (ConversationTitleView) getSupportActionBar().getCustomView();
        buttonToggle = ViewUtil.findById(this, free.text.sms.R.id.button_toggle);
        sendButton = ViewUtil.findById(this, free.text.sms.R.id.send_button);
        attachButton = ViewUtil.findById(this, free.text.sms.R.id.attach_button);
        composeText = ViewUtil.findById(this, free.text.sms.R.id.embedded_text_editor);
        charactersLeft = ViewUtil.findById(this, free.text.sms.R.id.space_left);
        emojiToggle = ViewUtil.findById(this, free.text.sms.R.id.emoji_toggle);
        emojiDrawer = ViewUtil.findById(this, free.text.sms.R.id.emoji_drawer);
        unblockButton = ViewUtil.findById(this, free.text.sms.R.id.unblock_button);
        composePanel = ViewUtil.findById(this, free.text.sms.R.id.bottom_panel);
        composeBubble = ViewUtil.findById(this, free.text.sms.R.id.compose_bubble);
        container = ViewUtil.findById(this, free.text.sms.R.id.layout_container);

        if (SilencePreferences.isEmojiDrawerDisabled(this))
            emojiToggle.setVisibility(View.GONE);

        container.addOnKeyboardShownListener(this);

        int[] attributes = new int[]{free.text.sms.R.attr.conversation_item_bubble_background};
        TypedArray colors = obtainStyledAttributes(attributes);
        int defaultColor = colors.getColor(0, Color.WHITE);
        composeBubble.getBackground().setColorFilter(defaultColor, PorterDuff.Mode.MULTIPLY);
        colors.recycle();

        attachmentAdapter = new AttachmentTypeSelectorAdapter(this);
        attachmentManager = new AttachmentManager(this, this);

        SendButtonListener sendButtonListener = new SendButtonListener();
        ComposeKeyPressedListener composeKeyPressedListener = new ComposeKeyPressedListener();

        emojiToggle.attach(emojiDrawer);
        emojiToggle.setOnClickListener(new EmojiToggleListener());
        emojiDrawer.setEmojiEventListener(new EmojiDrawer.EmojiEventListener() {
            @Override
            public void onKeyEvent(KeyEvent keyEvent) {
                composeText.dispatchKeyEvent(keyEvent);
            }

            @Override
            public void onEmojiSelected(String emoji) {
                composeText.insertEmoji(emoji);
            }
        });

        composeText.setOnEditorActionListener(sendButtonListener);
        attachButton.setOnClickListener(new AttachButtonListener());
        attachButton.setOnLongClickListener(new AttachButtonLongClickListener());
        sendButton.setOnClickListener(sendButtonListener);
        sendButton.setEnabled(true);
        sendButton.addOnTransportChangedListener(new OnTransportChangedListener() {
            @Override
            public void onChange(TransportOption newTransport, boolean manuallySelected) {
                calculateCharactersRemaining();
                composeText.setTransport(newTransport);
                buttonToggle.getBackground().setColorFilter(newTransport.getBackgroundColor(), Mode.MULTIPLY);
                buttonToggle.getBackground().invalidateSelf();
                if (manuallySelected) {
                    recordSubscriptionIdPreference(newTransport.getSimSubscriptionId());
                }
            }
        });

        titleView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConversationActivity.this, RecipientPreferenceActivity.class);
                intent.putExtra(RecipientPreferenceActivity.RECIPIENTS_EXTRA, recipients.getIds());

                startActivitySceneTransition(intent, titleView.findViewById(free.text.sms.R.id.title), "recipient_name");
            }
        });

        unblockButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                handleUnblock();
            }
        });

        composeText.setOnKeyListener(composeKeyPressedListener);
        composeText.addTextChangedListener(composeKeyPressedListener);
        composeText.setOnEditorActionListener(sendButtonListener);
        composeText.setOnClickListener(composeKeyPressedListener);
        composeText.setOnFocusChangeListener(composeKeyPressedListener);
    }

    protected void initializeActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(free.text.sms.R.layout.conversation_title_view);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    private void initializeResources() {
        if (recipients != null) recipients.removeListener(this);

        recipients = RecipientFactory.getRecipientsForIds(this, getIntent().getLongArrayExtra(RECIPIENTS_EXTRA), true);
        threadId = getIntent().getLongExtra(THREAD_ID_EXTRA, -1);
        archived = getIntent().getBooleanExtra(IS_ARCHIVED_EXTRA, false);
        distributionType = getIntent().getIntExtra(DISTRIBUTION_TYPE_EXTRA, ThreadDatabase.DistributionTypes.DEFAULT);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            LinearLayout conversationContainer = ViewUtil.findById(this, free.text.sms.R.id.conversation_container);
            conversationContainer.setClipChildren(true);
            conversationContainer.setClipToPadding(true);
        }

        recipients.addListener(this);
    }

    @Override
    public void onModified(final Recipients recipients) {
        titleView.post(new Runnable() {
            @Override
            public void run() {
                titleView.setTitle(recipients);
                setBlockedUserState(recipients);
                setActionBarColor(recipients.getColor());
                updateRecipientPreferences();
            }
        });
    }

    private void initializeReceivers() {
        securityUpdateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                long eventThreadId = intent.getLongExtra("thread_id", -1);

                if (eventThreadId == threadId || eventThreadId == -2) {
                    initializeSecurity();
                    updateRecipientPreferences();
                    calculateCharactersRemaining();
                }
            }
        };

        groupUpdateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.w("ConversationActivity", "Group update received...");
                if (recipients != null) {
                    long[] ids = recipients.getIds();
                    Log.w("ConversationActivity", "Looking up new recipients...");
                    recipients = RecipientFactory.getRecipientsForIds(context, ids, true);
                    recipients.addListener(ConversationActivity.this);
                    titleView.setTitle(recipients);
                }
            }
        };

        registerReceiver(securityUpdateReceiver,
                new IntentFilter(SecurityEvent.SECURITY_UPDATE_EVENT),
                KeyCachingService.KEY_PERMISSION, null);

        registerReceiver(groupUpdateReceiver,
                new IntentFilter(GroupDatabase.DATABASE_UPDATE_ACTION));
    }

    //////// Helper Methods

    private void addAttachment(int type) {
        Log.w("ComposeMessageActivity", "Selected: " + type);
        switch (type) {
            case AttachmentTypeSelectorAdapter.ADD_IMAGE:
                AttachmentManager.selectImage(this, PICK_IMAGE);
                break;
            case AttachmentTypeSelectorAdapter.ADD_VIDEO:
                AttachmentManager.selectVideo(this, PICK_VIDEO);
                break;
            case AttachmentTypeSelectorAdapter.ADD_SOUND:
                AttachmentManager.selectAudio(this, PICK_AUDIO);
                break;
            case AttachmentTypeSelectorAdapter.ADD_CONTACT_INFO:
                AttachmentManager.selectContactInfo(this, PICK_CONTACT_INFO);
                break;
            case AttachmentTypeSelectorAdapter.TAKE_PHOTO:
                attachmentManager.capturePhoto(this, TAKE_PHOTO);
                break;

        }
    }

    private void setMedia(Uri uri, MediaType mediaType) {
        attachmentManager.setMedia(masterSecret, uri, mediaType, getCurrentMediaConstraints());
    }

    private void addAttachmentContactInfo(Uri contactUri) {
        ContactAccessor contactDataList = ContactAccessor.getInstance();
        ContactData contactData = contactDataList.getContactData(this, contactUri);

        if (contactData.numbers.size() == 1) composeText.append(contactData.numbers.get(0).number);
        else if (contactData.numbers.size() > 1) selectContactInfo(contactData);
    }

    private void selectContactInfo(ContactData contactData) {
        final CharSequence[] numbers = new CharSequence[contactData.numbers.size()];
        final CharSequence[] numberItems = new CharSequence[contactData.numbers.size()];

        for (int i = 0; i < contactData.numbers.size(); i++) {
            numbers[i] = contactData.numbers.get(i).number;
            numberItems[i] = contactData.numbers.get(i).type + ": " + contactData.numbers.get(i).number;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIconAttribute(free.text.sms.R.attr.conversation_attach_contact_info);
        builder.setTitle(free.text.sms.R.string.ConversationActivity_select_contact_info);

        builder.setItems(numberItems, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                composeText.append(numbers[which]);
            }
        });
        builder.show();
    }

    private Drafts getDraftsForCurrentState() {
        Drafts drafts = new Drafts();

        if (!Util.isEmpty(composeText)) {
            drafts.add(new Draft(Draft.TEXT, composeText.getText().toString()));
        }

        for (Slide slide : attachmentManager.buildSlideDeck().getSlides()) {
            if (slide.hasAudio()) drafts.add(new Draft(Draft.AUDIO, slide.getUri().toString()));
            else if (slide.hasVideo())
                drafts.add(new Draft(Draft.VIDEO, slide.getUri().toString()));
            else if (slide.hasImage())
                drafts.add(new Draft(Draft.IMAGE, slide.getUri().toString()));
        }

        return drafts;
    }

    protected ListenableFuture<Long> saveDraft() {
        final SettableFuture<Long> future = new SettableFuture<>();

        if (this.recipients == null || this.recipients.isEmpty()) {
            future.set(threadId);
            return future;
        }

        final Drafts drafts = getDraftsForCurrentState();
        final long thisThreadId = this.threadId;
        final MasterSecret thisMasterSecret = this.masterSecret.parcelClone();
        final int thisDistributionType = this.distributionType;

        new AsyncTask<Long, Void, Long>() {
            @Override
            protected Long doInBackground(Long... params) {
                ThreadDatabase threadDatabase = DatabaseFactory.getThreadDatabase(ConversationActivity.this);
                DraftDatabase draftDatabase = DatabaseFactory.getDraftDatabase(ConversationActivity.this);
                long threadId = params[0];

                if (drafts.size() > 0) {
                    if (threadId == -1)
                        threadId = threadDatabase.getThreadIdFor(getRecipients(), thisDistributionType);

                    draftDatabase.insertDrafts(new MasterCipher(thisMasterSecret), threadId, drafts);
                    threadDatabase.updateSnippet(threadId, drafts.getSnippet(ConversationActivity.this),
                            drafts.getUriSnippet(ConversationActivity.this),
                            System.currentTimeMillis(), Types.BASE_DRAFT_TYPE, true);
                } else if (threadId > 0) {
                    threadDatabase.update(threadId, false);
                }

                return threadId;
            }

            @Override
            protected void onPostExecute(Long result) {
                future.set(result);
            }

        }.execute(thisThreadId);

        return future;
    }

    private void setActionBarColor(MaterialColor color) {
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(color.toActionBarColor(this)));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.setStatusBarColor(color.toStatusBarColor(this));
            window.setNavigationBarColor(getResources().getColor(android.R.color.black));
        }
    }

    private void setBlockedUserState(Recipients recipients) {
        if (recipients.isBlocked()) {
            unblockButton.setVisibility(View.VISIBLE);
            composePanel.setVisibility(View.GONE);
        } else {
            composePanel.setVisibility(View.VISIBLE);
            unblockButton.setVisibility(View.GONE);
        }
    }

    private void calculateCharactersRemaining() {
        int charactersSpent = composeText.getText().toString().length();
        TransportOption transportOption = sendButton.getSelectedTransport();
        CharacterState characterState = transportOption.calculateCharacters(charactersSpent);

        if (characterState.charactersRemaining <= 15 || characterState.messagesSpent > 1) {
            charactersLeft.setText(characterState.charactersRemaining + "/" + characterState.maxMessageSize
                    + " (" + characterState.messagesSpent + ")");
            charactersLeft.setVisibility(View.VISIBLE);
        } else {
            charactersLeft.setVisibility(View.GONE);
        }
    }

    private boolean isSingleConversation() {
        return getRecipients() != null && getRecipients().isSingleRecipient() && !getRecipients().isGroupRecipient();
    }

    private boolean isActiveGroup() {
        if (!isGroupConversation()) return false;

        try {
            byte[] groupId = GroupUtil.getDecodedId(getRecipients().getPrimaryRecipient().getNumber());
            GroupDatabase.GroupRecord record = DatabaseFactory.getGroupDatabase(this).getGroup(groupId);

            return record != null && record.isActive();
        } catch (IOException e) {
            Log.w("ConversationActivity", e);
            return false;
        }
    }

    private boolean isGroupConversation() {
        return getRecipients() != null &&
                (!getRecipients().isSingleRecipient() || getRecipients().isGroupRecipient());
    }

    private boolean isPushGroupConversation() {
        return getRecipients() != null && getRecipients().isGroupRecipient();
    }

    protected Recipients getRecipients() {
        return this.recipients;
    }

    protected long getThreadId() {
        return this.threadId;
    }

    private String getMessage() throws InvalidMessageException {

        String rawText = composeText.getText().toString() + signaTure();

        if (rawText.length() < 1 && !attachmentManager.isAttachmentPresent())
            throw new InvalidMessageException(getString(free.text.sms.R.string.ConversationActivity_message_is_empty_exclamation));

        if (!isEncryptedConversation &&
                AutoInitiate.isTaggableMessage(rawText) &&
                AutoInitiate.isTaggableDestination(getRecipients())) {
            rawText = AutoInitiate.getTaggedMessage(rawText);
        }

        return rawText;
    }

    String signaTure() {
        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(ConversationActivity.this);
        String siggy = "";

        //Nickname at the end of the message
        if (pref.getBoolean("pref_key_enable_nick_name", false)) {
            siggy = "\n\n" + pref.getString("pref_signature", "");
        }
        return siggy;
    }

    private MediaConstraints getCurrentMediaConstraints() {
        return MediaConstraints.MMS_CONSTRAINTS;
    }

    private void markThreadAsRead() {
        new AsyncTask<Long, Void, Void>() {
            @Override
            protected Void doInBackground(Long... params) {
                DatabaseFactory.getThreadDatabase(ConversationActivity.this).setRead(params[0]);
                MessageNotifier.updateNotification(ConversationActivity.this, masterSecret);
                return null;
            }
        }.execute(threadId);
    }

    protected void sendComplete(long threadId) {
        boolean refreshFragment = (threadId != this.threadId);
        this.threadId = threadId;

        if (fragment == null || !fragment.isVisible() || isFinishing()) {
            return;
        }

        if (refreshFragment) {
            fragment.reload(recipients, threadId);

            initializeSecurity();
            updateRecipientPreferences();
        }

        fragment.scrollToBottom();
        attachmentManager.cleanup();
    }

    private void sendMessage() {
        try {
            Recipients recipients = getRecipients();
            boolean forcePlaintext = sendButton.getSelectedTransport().isPlaintext();
            int subscriptionId = sendButton.getSelectedTransport().getSimSubscriptionId().or(-1);

            Log.w(TAG, "isManual Selection: " + sendButton.isManualSelection());
            Log.w(TAG, "forcePlaintext: " + forcePlaintext);

            if (recipients == null) {
                throw new RecipientFormattingException("Badly formatted");
            }

            if ((!recipients.isSingleRecipient() || recipients.isEmailRecipient()) && !isMmsEnabled) {
                handleManualMmsRequired();
            } else if (attachmentManager.isAttachmentPresent() || !recipients.isSingleRecipient() || recipients.isGroupRecipient() || recipients.isEmailRecipient()) {
                sendMediaMessage(forcePlaintext, subscriptionId);
            } else {
                sendTextMessage(forcePlaintext, subscriptionId);
            }

            if (isSentViaHttp()) {
                //enable/disable send button
                sendButton.setEnabled(false);
                new EnableSendButton().execute();
            }

        } catch (RecipientFormattingException ex) {
            Toast.makeText(ConversationActivity.this,
                    free.text.sms.R.string.ConversationActivity_recipient_is_not_a_valid_sms_or_email_address_exclamation,
                    Toast.LENGTH_LONG).show();
            Log.w(TAG, ex);
        } catch (InvalidMessageException ex) {
            Toast.makeText(ConversationActivity.this, free.text.sms.R.string.ConversationActivity_message_is_empty_exclamation,
                    Toast.LENGTH_SHORT).show();
            Log.w(TAG, ex);
        }
    }

    /*
        Delaying send button is for online sms only
    */
    class EnableSendButton extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            SharedPreferences pref = PreferenceManager
                    .getDefaultSharedPreferences(ConversationActivity.this);
            int delay = pref.getInt("sendDelay", 1);
            try {
                Thread.sleep(delay * 1000);
            } catch (Exception x) {
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void s) {
            sendButton.setEnabled(true);
        }

    }


    private void sendMediaMessage(final boolean forcePlaintext, final int subscriptionId)
            throws InvalidMessageException {
        final Context context = getApplicationContext();
        OutgoingMediaMessage outgoingMessage = new OutgoingMediaMessage(recipients,
                attachmentManager.buildSlideDeck(),
                getMessage(),
                System.currentTimeMillis(),
                subscriptionId,
                distributionType);

        if (isEncryptedConversation && !forcePlaintext) {
            outgoingMessage = new OutgoingSecureMediaMessage(outgoingMessage);
        }

        attachmentManager.clear();
        composeText.setText("");

        new AsyncTask<OutgoingMediaMessage, Void, Long>() {
            @Override
            protected Long doInBackground(OutgoingMediaMessage... messages) {
                return MessageSender.send(context, masterSecret, messages[0], threadId, true);
            }

            @Override
            protected void onPostExecute(Long result) {
                sendComplete(result);
            }
        }.execute(outgoingMessage);
    }

    private void sendTextMessage(boolean forcePlaintext, final int subscriptionId)
            throws InvalidMessageException {
        final Context context = getApplicationContext();
        OutgoingTextMessage message;

        if (isEncryptedConversation && !forcePlaintext) {
            message = new OutgoingEncryptedMessage(recipients, getMessage(), subscriptionId);
        } else {
            message = new OutgoingTextMessage(recipients, getMessage(), subscriptionId);
        }

        this.composeText.setText("");

        new AsyncTask<OutgoingTextMessage, Void, Long>() {
            @Override
            protected Long doInBackground(OutgoingTextMessage... messages) {
                return MessageSender.send(context, masterSecret, messages[0], threadId, true);
            }

            @Override
            protected void onPostExecute(Long result) {
                sendComplete(result);
            }
        }.execute(message);
    }


    private void updateToggleButtonState() {
        if (composeText.getText().length() == 0 && !attachmentManager.isAttachmentPresent()) {
            buttonToggle.display(attachButton);
        } else {
            buttonToggle.display(sendButton);
        }
    }

    private void recordSubscriptionIdPreference(final Optional<Integer> subscriptionId) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                DatabaseFactory.getRecipientPreferenceDatabase(ConversationActivity.this)
                        .setDefaultSubscriptionId(recipients, subscriptionId.or(-1));
                return null;
            }
        }.execute();
    }


    // Listeners

    private class AttachmentTypeListener implements DialogInterface.OnClickListener {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            addAttachment(attachmentAdapter.buttonToCommand(which));
            dialog.dismiss();
        }
    }

    private class EmojiToggleListener implements OnClickListener {

        @Override
        public void onClick(View v) {
            if (container.getCurrentInput() == emojiDrawer) container.showSoftkey(composeText);
            else container.show(composeText, emojiDrawer);
        }
    }

    private class SendButtonListener implements OnClickListener, TextView.OnEditorActionListener {
        @Override
        public void onClick(View v) {
            sendMessage();
        }

        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_SEND) {
                sendButton.performClick();
                return true;
            }
            return false;
        }
    }

    private class AttachButtonListener implements OnClickListener {
        @Override
        public void onClick(View v) {
            handleAddAttachment();
        }
    }

    private class AttachButtonLongClickListener implements View.OnLongClickListener {
        @Override
        public boolean onLongClick(View v) {
            return sendButton.performLongClick();
        }
    }

    private class ComposeKeyPressedListener implements OnKeyListener, OnClickListener, TextWatcher, OnFocusChangeListener {

        int beforeLength;

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (SilencePreferences.getEnterKeyType(ConversationActivity.this).equals("send")) {
                        sendButton.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER));
                        sendButton.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_ENTER));
                        return true;
                    }
                }
            }
            return false;
        }

        @Override
        public void onClick(View v) {
            container.showSoftkey(composeText);
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            beforeLength = composeText.getText().length();
        }

        @Override
        public void afterTextChanged(Editable s) {
            calculateCharactersRemaining();

            if (composeText.getText().length() == 0 || beforeLength == 0) {
                composeText.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        updateToggleButtonState();
                    }
                }, 50);
            }
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
        }
    }

    @Override
    public void setThreadId(long threadId) {
        this.threadId = threadId;
    }

    @Override
    public void onAttachmentChanged() {
        initializeSecurity();
        updateRecipientPreferences();
        updateToggleButtonState();
    }

    private class RecipientPreferencesTask extends AsyncTask<Recipients, Void, Pair<Recipients,
            RecipientPreferenceDatabase.RecipientsPreferences>> {
        @Override
        protected Pair<Recipients, RecipientPreferenceDatabase.RecipientsPreferences>
        doInBackground(Recipients... recipients) {
            if (recipients.length != 1 || recipients[0] == null) {
                throw new AssertionError("task needs exactly one Recipients object");
            }

            Optional<RecipientPreferenceDatabase.RecipientsPreferences> prefs = DatabaseFactory
                    .getRecipientPreferenceDatabase(ConversationActivity.this)
                    .getRecipientsPreferences(recipients[0].getIds());
            return new Pair<>(recipients[0], prefs.orNull());
        }

        @Override
        protected void onPostExecute(@NonNull Pair<Recipients, RecipientPreferenceDatabase
                .RecipientsPreferences> result) {
            if (result.first == recipients) {
                updateDefaultSubscriptionId(result.second != null ? result.second
                        .getDefaultSubscriptionId() : SubscriptionManagerCompat.getDefaultMessagingSubscriptionId());
            }
        }
    }

           /* Monetization section start*/

    boolean ads() {

        SharedPreferences pref = getSharedPreferences(
                "adnetwork", 0);
        SharedPreferences.Editor editor = pref.edit();
        int istimeToShowAd = 4;

        int adDisplayCounter = pref.getInt("calledTimes", 0);
        adDisplayCounter = adDisplayCounter + 1;
        editor.putInt("calledTimes", adDisplayCounter).apply();

        if (adDisplayCounter >= istimeToShowAd) {
            editor.putInt("calledTimes", 0).apply();
            Log.i(TAG, "We can display ad now");
            return true;
        } else {
            Log.i(TAG, "Not the time to show ads: " + adDisplayCounter + "/" + istimeToShowAd);
        }
        return false;
    }

    void interstitial_15() {
        try {
            mInterstitialAd = new InterstitialAd(ConversationActivity.this);
            mInterstitialAd.setAdUnitId(getString(R.string.aunit_15));

            AdRequest adRequest = new AdRequest.Builder().build();
            mInterstitialAd.loadAd(adRequest);

            mInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                }

                @Override
                public void
                onAdLeftApplication() {
                    Log.i(TAG, "Ad unit 15 clicked");
                }

                @Override
                public void onAdFailedToLoad(int errorCode) {
                    interstitial_7_5();
                    Log.i(TAG, "ad unit 15 failed to load");
                }

                @Override
                public void onAdLoaded() {
                    mInterstitialAd.show();
                    Log.i(TAG, "Ad unit 15 loaded");
                }
            });


        } catch (Exception x) {

        }
    }

    void interstitial_7_5() {
        try {

            mInterstitialAd = new InterstitialAd(ConversationActivity.this);
            mInterstitialAd.setAdUnitId(getString(R.string.aunit_7_5));

            AdRequest adRequest = new AdRequest.Builder().build();
            mInterstitialAd.loadAd(adRequest);

            mInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                }

                @Override
                public void
                onAdLeftApplication() {
                    Log.i(TAG, "Ad unit 7.5 clicked");
                }

                @Override
                public void onAdFailedToLoad(int errorCode) {
                    interstitial_2_5();
                    Log.i(TAG, "ad unit 7.5 failed to load");
                }

                @Override
                public void onAdLoaded() {
                    mInterstitialAd.show();
                    Log.i(TAG, "Ad unit 7.5 loaded");
                }
            });


        } catch (Exception x) {

        }
    }

    void interstitial_2_5() {
        try {
            mInterstitialAd = new InterstitialAd(ConversationActivity.this);
            mInterstitialAd.setAdUnitId(getString(R.string.aunit_2_25));

            AdRequest adRequest = new AdRequest.Builder().build();
            mInterstitialAd.loadAd(adRequest);

            mInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                }

                @Override
                public void
                onAdLeftApplication() {
                    Log.i(TAG, "Ad unit 2.5 clicked");
                }

                @Override
                public void onAdFailedToLoad(int errorCode) {
                    interstitial_1_00();
                    Log.i(TAG, "ad unit 2.5 failed to load");
                }

                @Override
                public void onAdLoaded() {
                    mInterstitialAd.show();
                    Log.i(TAG, "Ad unit 2.5 loaded");
                }
            });

        } catch (Exception x) {

        }
    }

    void interstitial_1_00() {
        try {

            mInterstitialAd = new InterstitialAd(ConversationActivity.this);
            mInterstitialAd.setAdUnitId(getString(R.string.aunit_1_00));

            AdRequest adRequest = new AdRequest.Builder().build();
            mInterstitialAd.loadAd(adRequest);

            mInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                }

                @Override
                public void
                onAdLeftApplication() {
                    Log.i(TAG, "Ad unit 1.0 clicked");
                }

                @Override
                public void onAdFailedToLoad(int errorCode) {
                    //interstitial_appodeal();
                    Log.i(TAG, "ad unit 1.0 failed to load");
                }

                @Override
                public void onAdLoaded() {
                    mInterstitialAd.show();
                    Log.i(TAG, "Ad unit 1.0 loaded");
                }
            });


        } catch (Exception x) {

        }
    }

    void pollfish() {
        PollFish.ParamsBuilder paramsBuilder = new PollFish.ParamsBuilder(getString(R.string.pFishKey))
                .indicatorPosition(Position.MIDDLE_RIGHT).pollfishSurveyReceivedListener(new PollfishSurveyReceivedListener() {
                    @Override
                    public void onPollfishSurveyReceived(final boolean playfulSurvey, final int surveyPrice) {
                        //         Log.i(TAG, "Pollfish survey received");
                    }
                }).pollfishSurveyCompletedListener(new PollfishSurveyCompletedListener() {
                    @Override
                    public void onPollfishSurveyCompleted(final boolean playfulSurvey, final int surveyPrice) {

/*                        //Getting the current date and time
                        Calendar c = Calendar.getInstance();
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy hh:mm:ss");
                        String timenow = simpleDateFormat.format(c.getTime());

                        //Saving the date and time when the survey is completed
                        SharedPreferences pref = mContext.getSharedPreferences(
                                "adnetwork", 0);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("expiration", timenow).apply();
                        PollFish.hide();
                        Log.i(TAG, "Pollfish survey completed");*/
                    }
                }).pollfishOpenedListener(new PollfishOpenedListener() {
                    @Override
                    public void onPollfishOpened() {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ConversationActivity.this);
                        alertDialogBuilder.setMessage(R.string.survey_dialog_message);

/*                        alertDialogBuilder.setNegativeButton("Not Now", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                PollFish.hide();
                            }
                        });*/

                        alertDialogBuilder.setPositiveButton("Start Survey", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                PollFish.show();
                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.setTitle(R.string.survey_dialog_title);
                        alertDialog.setIcon(R.drawable.icon);
                        alertDialog.setCanceledOnTouchOutside(false);
                        alertDialog.setCancelable(false);
                        alertDialog.show();
                    }
                }).pollfishSurveyNotAvailableListener(new PollfishSurveyNotAvailableListener() {
                    @Override
                    public void onPollfishSurveyNotAvailable() {
                        if (ads())
                            interstitial_15();

                    }
                }).customMode(true).build();
        PollFish.initWith(ConversationActivity.this, paramsBuilder);
    }



  /*  void interstitial_appodeal() {
        try {

            Appodeal.disableNetwork(ConversationActivity.this, "applovin");
            Appodeal.disableNetwork(ConversationActivity.this, "chartboost");
            Appodeal.disableNetwork(ConversationActivity.this, "flurry");
            Appodeal.disableNetwork(ConversationActivity.this, "unity_ads");
            Appodeal.disableNetwork(ConversationActivity.this, "mailru");
            Appodeal.disableNetwork(ConversationActivity.this, "adcolony");
            Appodeal.disableNetwork(ConversationActivity.this, "vungle");
            Appodeal.disableNetwork(ConversationActivity.this, "yandex");
            Appodeal.disableNetwork(ConversationActivity.this, "avocarrot");
            Appodeal.disableNetwork(ConversationActivity.this, "cheetah");
            Appodeal.disableNetwork(ConversationActivity.this, "revmob");
            Appodeal.disableNetwork(ConversationActivity.this, "tapjoy");

            Appodeal.setAutoCache(Appodeal.INTERSTITIAL, false);
            Appodeal.initialize(ConversationActivity.this, getString(R.string.apokey), Appodeal.INTERSTITIAL);
            Appodeal.cache(ConversationActivity.this, Appodeal.INTERSTITIAL);

            Appodeal.setInterstitialCallbacks(new InterstitialCallbacks() {

                public void onInterstitialLoaded(boolean isPrecache) {
                    Appodeal.show(ConversationActivity.this, Appodeal.INTERSTITIAL);
                }

                public void onInterstitialFailedToLoad() {
                }

                public void onInterstitialShown() {
                }

                public void onInterstitialClicked() {
                }

                public void onInterstitialClosed() {
                }
            });


        } catch (Exception x) {
        }
    } */
}
