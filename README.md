# Freedom SMS: Free Text To PH
[Freedom SMS: Free Text To PH  FSMS: Free Text To PH](https://play.google.com/store/apps/details?id=free.text.sms) is a modified code of Silence (formerly SMSSecure). While the main focus of Silence is secure messaging, the main purpose of this app is to act as SMS gateway and provide Free SMS  to Philippines.



Sending SMS across the globe is expensive so FSMS's goal is to give free SMS to everyone who wants to keep in touch with their family back in the Philippines.


FSMS: Free Text To PH works like any other SMS application. There's nothing to sign up for and no new service you need to Join. Just install and send free SMS to Philippines.

# Notes 
* The server source code is not included in this repo. Anyone who wants to re-compile this source should build their own SMS server.
* Instructions on how to insert your SMS API. Look these lines in SmsSendJob.java



```
#!java

    class Params {
        private String _text;
        private String _address;
        private SmsMessageRecord _message;
        private MasterSecret _masterSecret;

        Params(String text, String address, SmsMessageRecord message, MasterSecret masterSecret) {
            this._text = text;
            this._address = address;
            this._message = message;
            this._masterSecret = masterSecret;
        }
    }

    class HttpSmsTask extends AsyncTask<Params, String, String> {

        JSONObject json = null;
        JSONParser jsonParser = new JSONParser();
        SmsMessageRecord message;
        MasterSecret masterSecret;
        String address;
       
        int version;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Params... params) {


            String text = params[0]._text;
            address = params[0]._address;
            message = params[0]._message;
            masterSecret = params[0]._masterSecret;
    
            /*
               Do something here to push the message to your server
               use HTTP POST and use the JSONParser class
            */

           
            return null;
        }

        protected void onPostExecute(String s) {
          /*
            Do something here to handle your server's reponse.
            Handle JSON objects here and show something to the
            user that the message was sent successfully or not
            
            
            MARK MESSAGE AS SENT:
            context.sendBroadcast(constructSentIntent(context, messageId, message.getType(),
                            false, false));
            ELSE FAILED:
            markMessageFailed(JSONObject json, MasterSecret masterSecret)
            
          */ 
        }
    }

```



# License

Licensed under the GPLv3: http://www.gnu.org/licenses/gpl-3.0.html